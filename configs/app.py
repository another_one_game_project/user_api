import enum
import logging
import os
from logging.config import dictConfig

from decouple import AutoConfig
from pydantic import BaseSettings

APP_DIR: str = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
CONFIG_ENV: str = os.getenv('CONFIG_ENV', '')

if not CONFIG_ENV:
    raise RuntimeError(
        'What is your environment local, prod or qa. '
        'You need to specify an environment variable "export CONFIG_ENV=?"'
    )

CONFIG_DIR = os.path.join(APP_DIR, 'configs', CONFIG_ENV, '.env')
auth_config = AutoConfig(search_path=CONFIG_DIR)


class Env(str, enum.Enum):
    """Common environment names"""

    QA = 'qa'
    LOCAL = 'local'


class Settings(BaseSettings):
    CONFIG_ENV: Env = CONFIG_ENV
    VERSION: str = '0.0.1'
    SERVICE_NAME: str = 'user_api'
    DEBUG: bool = False
    LOGGER_LEVEL: str = 'INFO'
    ENVIRONMENT: str = auth_config('ENVIRONMENT', cast=str)
    DATABASE_DSN: str
    REDIS_DSN: str

    USER_API_AUTH = auth_config('USER_API_AUTH', cast=str)

    TIMEOUT: int = 10

    @property
    def logging_settings(self):
        return {
            'disable_existing_loggers': False,
            'version': 1,
            'formatters': {
                'basic': {
                    'format': (
                        r'[%(asctime)s] %(levelname)s '
                        r'in {%(module)s} - %(message)s'
                    )
                }
            },
            'handlers': {
                'stream': {
                    'class': 'logging.StreamHandler',
                    'formatter': 'basic',
                    'level': logging.getLevelName(self.LOGGER_LEVEL),
                },
            },
            'loggers': {
                '': {
                    'handlers': ['stream'],
                    'level': logging.getLevelName(self.LOGGER_LEVEL),
                    'propagate': False,
                },
            },
            'root': {
                'handlers': ['stream'],
                'level': logging.getLevelName(self.LOGGER_LEVEL),
                'propagate': False,
            },
        }

    class Config:
        env_file = CONFIG_DIR


config = Settings()
dictConfig(config.logging_settings)
