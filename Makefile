CONFIG_ENV := $(or ${CONFIG_ENV},${CONFIG_ENV},'')
APP_PORT := $(or ${APP_PORT},${APP_PORT},5000)
APP_HOST := $(or ${APP_HOST},${APP_HOST},0.0.0.0)
APP_WORKER := $(or ${APP_WORKER},${APP_WORKER},1)
PYTHONPATH := $(or ${PYTHONPATH},${PYTHONPATH},.)


.PHONY: isort
isort:
	@echo -n "Run isort"
	isort user_api
	isort configs


.flake: $(shell find user_api -type f) \
		$(shell find configs -type f)
	flake8 --ignore=F403,F405 --exclude versions user_api configs
	@if ! isort -c user_api configs; then \
            echo "Import sort errors, run 'make isort' to fix them!!!"; \
            isort --diff user_api configs; \
            false; \
	fi


.PHONY: flake
flake: .flake
	@echo -n "Run flake and isort"
	@echo ""


.PHONY: run-local
run-local:
	@echo "Run user_api local, env_file=${ENV_FILE}"
	@uvicorn user_api.app:app --reload --host ${APP_HOST} --port ${APP_PORT}


.PHONY: run-local-docker
run-local-docker:
	@echo "Run user_api. qa config"
	@docker-compose run user_api


.PHONY: build-local-docker
build-local-docker:
	@echo "Manual build. fix dns problem."
	@docker build -t user_api:local .


.PHONY: test
test:
	@echo "Run test in docker-composer, config_env=${CONFIG_ENV}"
	docker-compose -f docker-compose.yaml run --rm test


.PHONY: test-local
test-local:
	@echo "Run test local, config_env=${CONFIG_ENV}"
	python -m pytest -vvv


.PHONY: test-ci
test-ci:
	@echo "Run test ci flag config_env=${CONFIG_ENV}"
	python -m pytest -vvv --full-trace --cov-report term --cov=user_api


.PHONY: clean-pip
clean-pip:
	@echo -n "Clear python requirements for current library"
	pip freeze | grep -v "^-e" | xargs pip uninstall -y


.PHONY: alembic-autogenerate
alembic-autogenerate:
	@echo -n "Alembic Autogenerate alembic revision --autogenerate"
	alembic revision --autogenerate


.PHONY: alembic-upgrade
alembic-upgrade:
	@echo -n "Alembic Upgrade alembic upgrade head"
	alembic upgrade head


.PHONY: clean
clean:
	@echo -n "Clear temp files"
	@echo -n "\n"
	@rm -rf `find . -name __pycache__`
	@rm -rf `find . -type f -name '*.py[co]' `
	@rm -rf `find . -type f -name '*~' `
	@rm -rf `find . -type f -name '.*~' `
	@rm -rf `find . -type f -name '@*' `
	@rm -rf `find . -type f -name '#*#' `
	@rm -rf `find . -type f -name '*.orig' `
	@rm -rf `find . -type f -name '*.rej' `
	@rm -rf .coverage
	@rm -rf coverage.html
	@rm -rf coverage.xml
	@rm -rf htmlcov
	@rm -rf build
	@rm -rf cover
	@rm -rf .develop
	@rm -rf .flake
	@rm -rf .install-deps
	@rm -rf *.egg-info
	@rm -rf .pytest_cache
	@rm -rf dist


.PHONY: help
help:
	@echo -n "Common make targets"
	@echo ":"
	@cat Makefile | sed -n '/^\.PHONY: / h; /\(^\t@*echo\|^\t:\)/ {H; x; /PHONY/ s/.PHONY: \(.*\)\n.*"\(.*\)"/  make \1\t\2/p; d; x}'| sort -k2,2 |expand -t 20

