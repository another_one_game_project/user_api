import logging
import uuid

import pytest
from asgi_lifespan import LifespanManager
from httpx import AsyncClient
from starlette import status

from user_api.app import app

logger = logging.getLogger(__name__)


@pytest.fixture
async def httpxclient(caplog):
    # TODO: https://github.com/encode/httpx/issues/350
    async with LifespanManager(app):
        async with AsyncClient(app=app, base_url="http://localhost") as client:
            yield client


@pytest.mark.anyio
async def test_homepage_with_httpxclient(httpxclient):
    url = app.url_path_for(f"/user/{str(uuid.uuid4())}")
    resp = await httpxclient.get(url)
    assert resp.status_code == status.HTTP_404_NOT_FOUND
