import asyncio
import logging
from collections import Awaitable
from typing import Mapping, Tuple

from aiopg.pool import Pool
from aiopg.sa import Engine, create_engine
from aioredis import Redis
from fastapi import FastAPI
from sqlalchemy.engine.url import make_url as _make_url

logger = logging.getLogger(__name__)


class BaseStatus:
    async def get_status_info(self) -> Tuple[Mapping, bool]:
        raise NotImplementedError()


class PSTException(Exception):
    """Default exception pst"""

    @classmethod
    def from_cause(cls, *args, causes=None, **kwargs):
        self = cls(*args, **kwargs)
        self.__exception_cause(causes or [])

    def __exception_cause(self, causes):
        error_iter = None
        for ex in reversed(causes):
            error_iter = error_iter.__cause__ if error_iter else self
            error_iter.__cause__ = ex


class DBException(PSTException):
    """"""


class CreateEngineDBException(DBException):
    """"""


class CreateEnginesDBException(CreateEngineDBException):
    """"""


class CloseEngineDBException(DBException):
    """"""


class CloseEnginesDBException(CloseEngineDBException):
    """"""


class MyFastAPI(FastAPI):
    @property
    def db(self) -> Pool:
        return self.extra['db']

    @db.setter
    def db(self, pool: Pool) -> None:
        self.extra['db'] = pool

    @property
    def redis(self) -> Redis:
        return self.extra['redis']

    @redis.setter
    def redis(self, redis: Redis) -> None:
        self.extra['redis'] = redis


def make_url(conf_db):
    if isinstance(conf_db, str):
        conf_url = _make_url(conf_db)

        conf = {
            'username': conf_url.username,
            'database': conf_url.database,
            'port': conf_url.port,
            'host': conf_url.host,
            'password': conf_url.password,
            'db_alias': 'user_api_db'
        }
    else:
        conf = conf_db

    return conf


class EngineDB(BaseStatus, Awaitable):
    def __init__(
            self, username, database, port, host, password, db_alias,
            loop=None, timeout=30, enable_echo=False, enable_hstore=False,
            pool_recycle=-1, pool_minsize=1, pool_maxsize=10,
            application_name=''
    ):
        self._loop = loop or asyncio.get_event_loop()
        self._username = username
        self._port = port
        self._database = database
        self._host = host
        self._password = password
        self._timeout = timeout
        self._enable_echo = enable_echo
        self._enable_hstore = enable_hstore
        self._pool_recycle = pool_recycle
        self._pool_minsize = pool_minsize
        self._pool_maxsize = pool_maxsize
        self._application_name = application_name
        self._db_alias = db_alias
        self._engine = None

    async def get_status_info(self):
        status = True
        try:
            async with self.engine.acquire() as conn:
                resp = await conn.execute('select version();')
                summary = {
                    'pool': {
                        'min': self.engine.minsize,
                        'max': self.engine.maxsize,
                        'free': self.engine.maxsize - self.engine.size,
                        'use': self.engine.size,
                    },
                    'version': await resp.scalar(),
                    'application_name': self._application_name,
                    'status': 'OK',
                }
                status &= True

        except Exception as e:
            summary = 'ERROR'
            status &= False
            logger.exception(e)

        return summary, status

    async def create_engine(self):
        """

        :return: self
        """
        if self.is_engine:
            return self

        try:
            self._engine = await create_engine(
                user=self._username,
                database=self._database,
                host=self._host,
                port=self._port,
                password=self._password,
                loop=self._loop,
                timeout=self._timeout,
                pool_recycle=self._pool_recycle,
                echo=self._enable_echo,
                minsize=self._pool_minsize,
                maxsize=self._pool_maxsize,
                enable_hstore=self._enable_hstore,
                application_name=self._application_name
            )

            return self
        except Exception as e:
            raise CreateEngineDBException(
                "Connect to database '{db_alias}': "
                "{database}@{host}:{port}".format(
                    db_alias=self._db_alias,
                    database=self._database,
                    host=self._host,
                    port=self._port,
                )
            ) from e

    async def close_engine(self):
        if self.is_engine:
            try:
                self._engine.close()
                await self._engine.wait_closed()
            except Exception as e:
                raise CloseEngineDBException(
                    "Close to database '{db_alias}': "
                    "{database}@{host}:{port}".format(
                        db_alias=self._db_alias,
                        database=self._database,
                        host=self._host,
                        port=self._port,
                    )
                ) from e
            finally:
                self._engine = None

    @property
    def is_engine(self):
        return isinstance(self._engine, Engine)

    @property
    def engine(self):
        return self._engine

    def __await__(self):
        return self.create_engine().__await__()
