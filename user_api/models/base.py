import logging
from typing import Dict, Union

import sqlalchemy as sa
from pydantic import BaseModel
from sqlalchemy.ext.declarative import declarative_base

logger = logging.getLogger(__name__)


def declarative_constructor(self, request):
    self.db = request.scope['app'].db.engine
    self.query = self.__table__
    self.c = self.query.c


class BaseModelCls(object):
    __basename__ = None

    def __init__(self, db):
        self.db = db
        self.query = None
        self.c = None

    async def execute(self, query):
        async with self.db.acquire() as conn:
            return await conn.execute(query)

    async def scalar(self, query):
        async with self.db.acquire() as conn:
            return await conn.scalar(query)

    async def all(self, query):
        async with self.db.acquire() as conn:
            res = await conn.execute(query)
            return await res.fetchall()

    async def fetchone(self, query):
        async with self.db.acquire() as conn:
            res = await conn.execute(query)
            return await res.fetchone()

    async def first(self, query):
        async with self.db.acquire() as conn:
            res = await conn.execute(query)
            return await res.first()

    async def insert(self, data, bulk=True):
        if not isinstance(data, (tuple, list, set)):
            data = [data]

        if not data:
            return

        if bulk:
            await self.execute(sa.insert(self.query).values(data))
        else:
            for row in data:
                try:
                    await self.execute(sa.insert(self.query).values(**row))
                except Exception as e:
                    logger.exception(e)

    async def insert_return_id(self, data):
        return await self.scalar(
            sa.insert(self.query).values(**data).returning(self.c.id)
        )

    async def update(self, data):
        for key, info in data.items():
            query = sa.update(self.query).where(self.query.columns.id == key)
            await self.execute(query.values(**info))


class BaseModelORM(BaseModel):
    @classmethod
    def from_sa(cls, data: Dict) -> Union['BaseModelORM', None]:
        return cls(**data) if data else None


Base = declarative_base(
    constructor=declarative_constructor,
    cls=BaseModelCls
)
