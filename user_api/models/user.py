import uuid
from datetime import datetime
from typing import Optional

import sqlalchemy as sa
from pydantic import EmailStr
from sqlalchemy.dialects.postgresql import UUID

from ..scheme import UserModelEmail, UserModelGet, UserModelId, UserModelUpdate
from .base import Base

__all__ = ['User']


class User(Base):
    __tablename__ = "custom_user"

    id = sa.Column(
        UUID(as_uuid=True),
        primary_key=True,
        index=True,
        default=uuid.uuid4
    )
    email = sa.Column(sa.String, unique=True, index=True)
    username = sa.Column(sa.String, nullable=False)
    date_joined = sa.Column(sa.DateTime, default=datetime.now)

    is_staff = sa.Column(sa.Boolean, default=False)
    is_active = sa.Column(sa.Boolean, default=True)
    is_deleted = sa.Column(sa.Boolean, default=False)

    async def get_by_id(
            self,
            user_id: uuid.UUID,
    ) -> Optional[UserModelGet]:
        query = sa.select(self.c).where(self.c.id == user_id)
        return UserModelGet.from_sa(await self.fetchone(query))

    async def get_by_email(
            self,
            user_email: EmailStr,
    ) -> Optional[UserModelGet]:
        query = sa.select(self.c).where(self.c.email == user_email)
        return UserModelGet.from_sa(await self.fetchone(query))

    async def insert_return_user_id(
            self,
            data: UserModelEmail
    ) -> UserModelId:
        query = sa.insert(self.query).values(data.dict()).returning(self.c.id)
        user_id = await self.fetchone(query)
        return UserModelId.from_sa(user_id)

    async def update_return_user(
            self,
            user_id: uuid.UUID,
            data: UserModelUpdate,
    ) -> UserModelGet:
        query = sa.update(self.query).where(
            self.query.columns.id == user_id
        )
        query = query.values(data.dict(
            exclude_none=True,
        )).returning(self.__table__)
        user = await self.fetchone(query)
        return UserModelGet.from_sa(user)
