import logging

import aioredis
from fastapi import FastAPI
from fastapi.responses import JSONResponse
from starlette.middleware.cors import CORSMiddleware

from configs.app import config

from .base_app import EngineDB, make_url
from .views import *

logger = logging.getLogger(__name__)

app = FastAPI(
    title=config.SERVICE_NAME,
    version=config.VERSION,
    debug=config.DEBUG,
)

app.add_middleware(
    CORSMiddleware,
    allow_origins=["*"],
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)

app.include_router(user_router, prefix='/user', tags=['user'])


@app.on_event("startup")
async def db_startup():
    app.db = await EngineDB(**make_url(config.DATABASE_DSN))


@app.on_event("shutdown")
async def db_shutdown():
    await app.db.close_engine()


@app.on_event("startup")
async def redis_startup():
    app.redis = await aioredis.from_url(config.REDIS_DSN)


@app.on_event("shutdown")
async def redis_shutdown():
    await app.redis.close()


@app.exception_handler(Exception)
async def common_exception_handler(request, e):
    logger.warning(e)
    return JSONResponse(status_code=500, content=str(e))
