import uuid
from datetime import date
from typing import Optional

from pydantic import BaseModel, EmailStr, constr

from user_api.models.base import BaseModelORM

__all__ = [
    'UserModelEmail',
    'UserModelId',
    'UserModelUsername',
    'UserModelService',
    'UserModelGet',
    'UserModelUpdate',
    'UserModelCreate',
]


class UserModelEmail(BaseModel):
    email: EmailStr


class UserModelUsername(BaseModel):
    username: constr(min_length=1)


class UserModelId(BaseModelORM):
    id: uuid.UUID


class UserModelService(BaseModel):
    is_staff: bool
    is_active: bool
    is_deleted: bool


class UserModelUpdate(BaseModel):
    email: Optional[EmailStr]
    username: Optional[constr(min_length=1)]
    is_staff: Optional[bool]
    is_active: Optional[bool]
    is_deleted: Optional[bool]


class UserModelCreate(UserModelEmail, UserModelUsername):
    pass


class UserModelGet(
    UserModelId,
    UserModelEmail,
    UserModelUsername,
    UserModelService,
):
    date_joined: date
