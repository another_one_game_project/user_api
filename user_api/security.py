import json
from typing import Optional

from fastapi import HTTPException
from fastapi.openapi.models import APIKey, APIKeyIn
from fastapi.security.base import SecurityBase
from pydantic import ValidationError
from starlette import status
from starlette.requests import Request

from configs.app import config
from user_api.base_app import MyFastAPI
from user_api.scheme import UserModelGet


class APIKeyHeader(SecurityBase):
    def __init__(self, *, name: str, group_name: Optional[str] = None):
        self.model: APIKey = APIKey(**{"in": APIKeyIn.header}, name=name)
        self.scheme_name = 'default'
        self.group_name = group_name

    async def __call__(self, request: Request) -> Optional[UserModelGet]:
        api_key: str = request.headers.get(self.model.name) or ''
        if not api_key:
            raise HTTPException(status_code=status.HTTP_401_UNAUTHORIZED)
        app: MyFastAPI = request.scope['app']
        try:
            key = api_key[7:]
            user_data = await app.redis.get(f'{config.USER_API_AUTH}{key}')
            if user_data:
                return UserModelGet(**json.loads(user_data))
        except (ValidationError, json.JSONDecodeError, TypeError):
            raise HTTPException(status_code=status.HTTP_401_UNAUTHORIZED)
        raise HTTPException(status_code=status.HTTP_401_UNAUTHORIZED)
