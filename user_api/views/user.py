import hashlib
import json
import logging
import uuid

from fastapi import APIRouter, HTTPException
from starlette import status
from starlette.requests import Request

from configs.app import config
from user_api.base_app import MyFastAPI
from user_api.dependencies import user_auth
from user_api.models.user import User
from user_api.scheme import (
    UserModelCreate,
    UserModelEmail,
    UserModelGet,
    UserModelId,
    UserModelUpdate,
)

__all__ = ['user_router']

user_router = APIRouter()

logger = logging.getLogger(__name__)


@user_router.get(
    '/{user_id}',
    response_model=UserModelGet,
)
async def get_user(user_id: uuid.UUID, request: Request):
    """
    Get user endpoint.
    :param user_id:
    :param request:
    :return:
    """
    app: MyFastAPI = request.scope['app']
    try:
        cached_user = await app.redis.get(f'{user_id}')
        if cached_user:
            return json.loads(cached_user)
    except Exception as e:
        logger.exception(f'[user_api.get_user] {e}')

    user_cls = User(request)
    try:
        user = await user_cls.get_by_id(user_id)
    except Exception as e:
        logger.exception(f'[user_api.get_user] {e}')
        raise HTTPException(status_code=status.HTTP_403_FORBIDDEN)
    if not user:
        raise HTTPException(
            status_code=status.HTTP_404_NOT_FOUND,
            detail="User not found"
        )
    await app.redis.set(f'{user_id}', user.json())
    return user


@user_router.post(
    '/',
    response_model=UserModelId,
    status_code=status.HTTP_201_CREATED,
)
async def create_user(user_data: UserModelCreate, request: Request):
    """
    Create user endpoint.
    :param user_data:
    :param request:
    :return:
    """
    user_cls = User(request)
    try:
        user_id = await user_cls.insert_return_user_id(user_data)
        return user_id
    except Exception as e:
        logger.exception(f'[user_api.create_user] {e}')
        raise HTTPException(status_code=status.HTTP_403_FORBIDDEN)


@user_router.put(
    '/{user_id}',
    response_model=UserModelGet,
)
async def update_user(
        user_id: uuid.UUID,
        data: UserModelUpdate,
        request: Request,
        user: UserModelGet = user_auth
):
    """
    Update user endpoint. Available only for authorized user.
    :param user_id:
    :param data:
    :param request:
    :param user:
    :return:
    """
    if not user_id == user.id:
        raise HTTPException(status_code=status.HTTP_401_UNAUTHORIZED)
    app: MyFastAPI = request.scope['app']
    user_cls = User(request)
    try:
        user = await user_cls.update_return_user(user_id, data)
        await app.redis.delete(f'{user_id}')
        if user:
            return user
    except Exception as e:
        logger.exception(f'[user_api.update_user] {e}')
        raise HTTPException(status_code=status.HTTP_403_FORBIDDEN)
    raise HTTPException(
        status_code=status.HTTP_404_NOT_FOUND,
        detail="User not found"
    )


@user_router.post('/login')
async def login_user(
        data: UserModelEmail,
        request: Request,
):
    """
    Login endpoint.
    :param data:
    :param request:
    :return:
    """
    user_cls = User(request)
    try:
        user = await user_cls.get_by_email(data.email)
    except Exception as e:
        logger.exception(f'[user_api.login_user] {e}')
        raise HTTPException(status_code=status.HTTP_403_FORBIDDEN)
    if not user or not user.is_active or user.is_deleted:
        raise HTTPException(
            status_code=status.HTTP_404_NOT_FOUND,
            detail="User not found"
        )
    key = hashlib.sha256(f'{user.email}'.encode('utf-8')).hexdigest()

    app: MyFastAPI = request.scope['app']
    try:
        await app.redis.set(
            f'{config.USER_API_AUTH}{key}',
            user.json(),
            ex=86400
        )
    except Exception as e:
        logger.exception(f'[user_api.login_user] {e}')
        raise HTTPException(status_code=status.HTTP_403_FORBIDDEN)

    return key
