from fastapi import Depends

from .security import APIKeyHeader

user_auth = Depends(APIKeyHeader(name='Authorization'))
