FROM python:3.8

RUN groupadd -r -g 30000 appuser && useradd -u 30000 -g 30000 --shell=/bin/bash appuser

ENV PYTHONPATH "/usr/local/user_api"

ENV MODULE_HOME="/usr/local/user_api"
RUN mkdir -p ${MODULE_HOME}/logs && mkdir -p ${MODULE_HOME}/external
WORKDIR "${MODULE_HOME}"

COPY --chown=appuser:appuser . .
RUN pip install --upgrade --no-cache-dir -r requirements.txt
USER appuser

ENTRYPOINT ["/usr/local/user_api/docker-entrypoint.sh"]
CMD ["help"]