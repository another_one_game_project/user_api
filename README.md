
Перед запуском необходимо иметь поднятую базу postgres и redis.
Настроить их подключение в configs/local/.env.
## Как запустить локально.
Установить зависимости
```bash
$ pip install -r requirements.txt
```
Произвести миграции бд командой 
```bash
$ PYTHONPATH=. CONFIG_ENV=local make alembic-upgrade.
```
Запустить приложение командой
```bash
$ CONFIG_ENV=local make run-local
```

## Как запустить docker
```bash
$ docker build -t api .

$ docker run --env CONFIG_ENV=local --network=host api:latest run-local
```